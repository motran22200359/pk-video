const webpack = require("webpack");

module.exports = {
  /*
   ** Headers of the page
   */
  configureWebpack: {
    devtool: 'source-map'
  },
  head: {
    title: "PKVideo",
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      {
        hid: "description",
        name: "description",
        content: "Singapore-based Real Estate Agent Search Engine"
      }
    ],
    script: [
      {
        src: "/js/jquery.min.js"
      },
      {
        src: "/js/bootstrap.min.js"
      }
    ],
    link: [
      { rel: "icon", type: "image/x-icon", href: "/favicon.ico" },
      {
        rel: "stylesheet",
        href: "/css/bootstrap.min.css"
      },
      {
        rel: "stylesheet",
        href: "/css/font-awesome2.min.css"
      },
      {
        rel: "stylesheet",
        href: "/css/montserrat.css"
      }
    ]
  },
  /*
   ** CSS files
   */
  css: ["~/assets/css/adjust-skins.css",
    "~/assets/css/main.css",
    "element-ui/lib/theme-chalk/index.css"
  ],

  modules: [
    "@nuxtjs/axios",
    "@nuxtjs/auth",
    "nuxt-material-design-icons",
    ['nuxt-fontawesome', {
      component: 'fa',
      imports: [
        // import whole set
        {
          set: '@fortawesome/free-solid-svg-icons',
          icons: ['fas']
        }
      ]
    }]
  ],

  axios: {
    // baseURL: "http://10.102.23.48:7777"
    // baseURL: "https://localhost:44359"
    // baseURL: "http://203.113.146.146:8884/"
    baseURL: "http://203.113.146.146:8885/"
    // baseURL: "http://video.pungkookvn.com:8885/"
  },

  env: {
    // iis local
    // baseUrl: process.env.BASE_URL || 'http://10.102.23.48:7777'
    // baseUrl: process.env.BASE_URL || 'https://localhost:44359'
    /* ----- server API anh Phuong create ----- */
    // baseUrl: process.env.BASE_URL || "http://203.113.146.146:8884/"
    /* ----- server API Dinh Van create ----- */
    baseUrl: process.env.BASE_URL || "http://203.113.146.146:8885/"
    /* ----- server API deploy new ----- */
    // baseUrl: process.env.BASE_URL || "http://video.pungkookvn.com:8885/"
  },

  auth: {
    redirect: {
      home: "/",
      login: "/auth/login",
      logout: "/", // after logout
      callback: "/login"
    },
    // resetOnError: true,//If enabled, user will be automatically logged out if any error happens. (For example when token expired)
    // watchLoggedIn: true,//When enabled (default) user will be redirected on login/logouts.
    strategies: {
      local: {
        endpoints: {
          // login: { url: 'auth/local', method: 'post', propertyName: 'jwt' },
          login: {
            url: "auth/local",
            method: "post",
            propertyName: "Result.token"
          },
          user: { url: "/users/me", method: "get", propertyName: false },
          logout: false
        }
      }
    }
  },

  // this will ensure every route need login
  router: {
    // comment middleware to display list video when not login
    // middleware: ["auth"]
  },

  /**
   * Plugins
   */
  plugins: [
    { src: "~/plugins/axios-airlisting", ssr: false },
    { src: "~/plugins/custom-local-storage", ssr: false },
    { src: "~/plugins/error-logger", ssr: false },
    { src: "~/plugins/element-ui", ssr: false },
    { src: "~/plugins/vue-clipboard2", ssr: false }
  ],
  /**
   * Rendering
   */
  render: {
    bundleRenderer: {
      shouldPreload: (file, type) => {
        return ["script", "style", "font"].includes(type);
      }
    }
  },
  /**
   * Mode
   */
  mode: "spa",
  /*
   ** Build configuration
   */
  build: {
    // analyze: true,
    plugins: [
      // set shortcuts as global for bootstrap
      new webpack.ProvidePlugin({
        $: "jquery",
        jQuery: "jquery",
        "window.jQuery": "jquery"
      })
    ],
    vendor: ["axios"],
    /* Run ESLint on save */
    extend(config, ctx) {
      if (ctx.dev && ctx.isClient) {
        config.module.rules.push({
          enforce: "pre",
          test: /\.(js|vue)$/,
          loader: "eslint-loader",
          exclude: /(node_modules)/,
          options: {
            fix: true
          }
        });
      }
      if (ctx.isDev) {
        config.devtool = ctx.isClient ? 'source-map' : 'inline-source-map'
      }
    }
  }
};
