import Vue from 'vue'
import Router from 'vue-router'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _80f2f82c = () => interopDefault(import('../pages/about.vue' /* webpackChunkName: "pages/about" */))
const _684a4c3b = () => interopDefault(import('../pages/contactUs.vue' /* webpackChunkName: "pages/contactUs" */))
const _77e66fef = () => interopDefault(import('../pages/gallery.vue' /* webpackChunkName: "pages/gallery" */))
const _238be46c = () => interopDefault(import('../pages/myvideo.vue' /* webpackChunkName: "pages/myvideo" */))
const _7a9695d6 = () => interopDefault(import('../pages/play-video.vue' /* webpackChunkName: "pages/play-video" */))
const _519e1125 = () => interopDefault(import('../pages/privacy.vue' /* webpackChunkName: "pages/privacy" */))
const _02997ec6 = () => interopDefault(import('../pages/profile.vue' /* webpackChunkName: "pages/profile" */))
const _753c83a0 = () => interopDefault(import('../pages/restaurant.vue' /* webpackChunkName: "pages/restaurant" */))
const _97675eca = () => interopDefault(import('../pages/search.vue' /* webpackChunkName: "pages/search" */))
const _5fd158a6 = () => interopDefault(import('../pages/template.vue' /* webpackChunkName: "pages/template" */))
const _2cf96937 = () => interopDefault(import('../pages/uploadvideo.vue' /* webpackChunkName: "pages/uploadvideo" */))
const _d5f2526a = () => interopDefault(import('../pages/auth/afterregister.vue' /* webpackChunkName: "pages/auth/afterregister" */))
const _61560299 = () => interopDefault(import('../pages/auth/confirmation.vue' /* webpackChunkName: "pages/auth/confirmation" */))
const _6a13c696 = () => interopDefault(import('../pages/auth/login.vue' /* webpackChunkName: "pages/auth/login" */))
const _4f880bda = () => interopDefault(import('../pages/auth/loginImpress.vue' /* webpackChunkName: "pages/auth/loginImpress" */))
const _3e42fb24 = () => interopDefault(import('../pages/auth/logout.vue' /* webpackChunkName: "pages/auth/logout" */))
const _3de2cc16 = () => interopDefault(import('../pages/auth/profile.vue' /* webpackChunkName: "pages/auth/profile" */))
const _d6b077f2 = () => interopDefault(import('../pages/auth/register.vue' /* webpackChunkName: "pages/auth/register" */))
const _523803af = () => interopDefault(import('../pages/manage-style/list-video.vue' /* webpackChunkName: "pages/manage-style/list-video" */))
const _511be224 = () => interopDefault(import('../pages/manage-style/upload.vue' /* webpackChunkName: "pages/manage-style/upload" */))
const _3edd1977 = () => interopDefault(import('../pages/setups/setup.vue' /* webpackChunkName: "pages/setups/setup" */))
const _81c36392 = () => interopDefault(import('../pages/treecomments/treecomment.vue' /* webpackChunkName: "pages/treecomments/treecomment" */))
const _637483e5 = () => interopDefault(import('../pages/users/user.vue' /* webpackChunkName: "pages/users/user" */))
const _d4c305b0 = () => interopDefault(import('../pages/editvideo/_videoid.vue' /* webpackChunkName: "pages/editvideo/_videoid" */))
const _064ce31e = () => interopDefault(import('../pages/video/_videoid.vue' /* webpackChunkName: "pages/video/_videoid" */))
const _418342a2 = () => interopDefault(import('../pages/index.vue' /* webpackChunkName: "pages/index" */))

// TODO: remove in Nuxt 3
const emptyFn = () => {}
const originalPush = Router.prototype.push
Router.prototype.push = function push (location, onComplete = emptyFn, onAbort) {
  return originalPush.call(this, location, onComplete, onAbort)
}

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: decodeURI('/'),
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/about",
    component: _80f2f82c,
    name: "about"
  }, {
    path: "/contactUs",
    component: _684a4c3b,
    name: "contactUs"
  }, {
    path: "/gallery",
    component: _77e66fef,
    name: "gallery"
  }, {
    path: "/myvideo",
    component: _238be46c,
    name: "myvideo"
  }, {
    path: "/play-video",
    component: _7a9695d6,
    name: "play-video"
  }, {
    path: "/privacy",
    component: _519e1125,
    name: "privacy"
  }, {
    path: "/profile",
    component: _02997ec6,
    name: "profile"
  }, {
    path: "/restaurant",
    component: _753c83a0,
    name: "restaurant"
  }, {
    path: "/search",
    component: _97675eca,
    name: "search"
  }, {
    path: "/template",
    component: _5fd158a6,
    name: "template"
  }, {
    path: "/uploadvideo",
    component: _2cf96937,
    name: "uploadvideo"
  }, {
    path: "/auth/afterregister",
    component: _d5f2526a,
    name: "auth-afterregister"
  }, {
    path: "/auth/confirmation",
    component: _61560299,
    name: "auth-confirmation"
  }, {
    path: "/auth/login",
    component: _6a13c696,
    name: "auth-login"
  }, {
    path: "/auth/loginImpress",
    component: _4f880bda,
    name: "auth-loginImpress"
  }, {
    path: "/auth/logout",
    component: _3e42fb24,
    name: "auth-logout"
  }, {
    path: "/auth/profile",
    component: _3de2cc16,
    name: "auth-profile"
  }, {
    path: "/auth/register",
    component: _d6b077f2,
    name: "auth-register"
  }, {
    path: "/manage-style/list-video",
    component: _523803af,
    name: "manage-style-list-video"
  }, {
    path: "/manage-style/upload",
    component: _511be224,
    name: "manage-style-upload"
  }, {
    path: "/setups/setup",
    component: _3edd1977,
    name: "setups-setup"
  }, {
    path: "/treecomments/treecomment",
    component: _81c36392,
    name: "treecomments-treecomment"
  }, {
    path: "/users/user",
    component: _637483e5,
    name: "users-user"
  }, {
    path: "/editvideo/:videoid?",
    component: _d4c305b0,
    name: "editvideo-videoid"
  }, {
    path: "/video/:videoid?",
    component: _064ce31e,
    name: "video-videoid"
  }, {
    path: "/",
    component: _418342a2,
    name: "index"
  }],

  fallback: false
}

export function createRouter () {
  return new Router(routerOptions)
}
