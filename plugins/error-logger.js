import Vue from 'vue';
import Raven from 'raven-js';
import RavenVue from 'raven-js/plugins/vue';

Raven.config('https://cd4c9d78588647a69a11723900d99599@sentry.io/167967')
  .addPlugin(RavenVue, Vue)
  .install();

var isRavenInitialized = false;

export function captureException (error) {
  var isLocalhost = (window.location.hostname === 'localhost' || window.location.hostname === '127.0.0.1');
  if (isLocalhost) {
    console.error(error);
  } else {
    if (!isRavenInitialized) {
      isRavenInitialized = true;
      window.onunhandledrejection = function(event) {
        Raven.captureException(event.reason);
      }
    }
    Raven.captureException(error);
  }
}
